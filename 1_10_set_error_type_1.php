<?php
/**
 * 设置错误级别的三种方法：
 *     通过修改PHP配置文件中的error_reporting选项的值
 *     通过error_reporting()函数设置
 *     通过ini_set()函数运行时设置
 */

// 显示当前的设置错误级别
echo error_reporting();

// 显示所有错误(也可以是error_reporting(-1);)
// error_reporting(E_ALL);

// 不显示提示的错误
// error_reporting(E_ALL & ~E_NOTICE);

// 不显示所有的错误（语法错误还是会显示的）
error_reporting(0);

// Notice错误
echo $test;
echo PHP_EOL;

// 致命错误
echo fun();
echo PHP_EOL;
