<?php
/**
 * error_log可以将日志写入到系统日志中去
 * 在计算机-管理-事件查看器-找到PHP7产生的日志-详细信息可以查看具体的错误
 */

// 设置开启所有的错误
error_reporting(-1);

// 关闭所有的错误显示
ini_set('display_errors', 0);

// 设置开启错误日志记录
ini_set('log_errors', 1);

// 错误日志保存到指定文件
ini_set('error_log', 'syslog');

// Warning 错误
settype($var, 'king');

// 致命错误
test();

echo PHP_EOL . '看看会不会运行' . PHP_EOL;