<?php
/**
 * E_WARNING，警告级别的错误，比如参数的类型错误，比如多传了一个参数等，并不影响后面代码的继续执行。
 */

settype($var, 'int');
var_dump($var);

settype($var, 'type');
var_dump($var);

echo "看看是否会执行";