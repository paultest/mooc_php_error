<?php
/**
 * 自定义错误处理器
 * 通知级别：写到日志里面去
 * 警告级别：发送邮件
 * 致命级别：发送邮件
 * User: jiand
 * Date: 2018/10/29
 * Time: 21:30
 */

class MyErrorHandler
{
    // 错误信息
    public $message = '';

    // 错误文件
    public $filename = '';

    // 错误行号
    public $line = 0;

    // 额外信息
    public $vars = [];

    // 通知的日志
    protected $_noticeLog = './logs/NoticeLog.log';

    public function __construct($message, $filename, $line , $vars)
    {
        $this->message = $message;
        $this->filename = $filename;
        $this->line = $line;
        $this->vars = $vars;
    }

    /**
     * 根据不同错误级别对应不同操作
     * @param $errno int 错误代码
     * @param $message
     * @param $file
     * @param $line int 错误行号
     * @param $vars
     * @return bool|void
     */
    public static function deal($errno, $message, $file, $line, $vars)
    {
        $self = new self($message, $file, $line, $vars);

        switch ($errno) {
            // 通知级别：写到日志里面去
            case E_NOTICE:
            case E_USER_NOTICE:
                return $self->dealNotice();
                break;
            // 警告级别：发送邮件
            case E_WARNING:
            case E_USER_WARNING:
                return $self->dealWarning();
                break;
            // 致命级别：发送邮件
            case E_ERROR:
            case E_USER_ERROR:
                return $self->dealError();
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * 如何处理通知错误：将错误写到日志里面去
     * @return bool
     */
    private function dealNotice()
    {
        $datetime = date('Y-m-d H:i:s');
        $errorMsg =
            <<<EOF
出现了通知错误，如下：
产生了通知的文件：{$this->filename}
产生了通知的信息：{$this->message}
产生了通知的行号：{$this->line}
产生了通知的时间：{$datetime}
EOF;
        return error_log($errorMsg, 3, $this->_noticeLog);
    }

    /**
     * 如何处理警告错误：将错误通过邮件的方式发送
     * @return bool
     */
    private function dealWarning()
    {
        $datetime = date('Y-m-d H:i:s');
        $errorMsg = <<<EOF
出现了警告错误，如下：
产生了警告的文件：{$this->filename}
产生了警告的信息：{$this->message}
产生了警告的行号：{$this->line}
产生了警告的时间：{$datetime}
EOF;
        return error_log($errorMsg, 1, '844865712@qq.com');
    }

    /**
     * 处理致命错误：将错误通过邮件的方式发送
     */
    private function dealError()
    {
        // 开启内存缓冲
        ob_start();

        // 回溯上一条信息
        debug_print_backtrace();

        // 得到缓存中的内容
        $back_trace = ob_get_flush();

        $datetime = date('Y-m-d H:i:s');
        $errorMsg = <<<EOF
            出现了致命错误，如下：
            产生了错误的文件：{$this->filename}
            产生了错误的信息：{$this->message}
            产生了错误的行号：{$this->line}
            产生了错误的时间：{$datetime}
            追踪信息：{$back_trace}
EOF;
        // 将错误通过邮件的方式发送
        error_log($errorMsg, 1, '844865712@qq.com');

        // 以错误代码形式中止
        exit(1);
    }
}