<?php
/**
 * 测试自定义错误处理器
 * User: jiand
 * Date: 2018/11/6
 * Time: 21:49
 */

header('content-type:text/html;charset=utf-8');

require_once 'MyErrorHandler.php';

error_reporting(-1);

/*
 * 接管系统的错误提示机制
 * 第一个参数是接管的函数，可以是数组，数组里面第一个是类名，第二个是方法名
 * 第二个参数是错误级别，如果不填的话，默认是所有的错误
 * set_error_handler(array(类名/new 类名，方法名));
 * set_error_handler(函数名);
 */
set_error_handler(['MyErrorHandler', 'deal']);

// E_NOTICE，通知级别的错误，会写到NoticeLog日志中去
echo $test;

echo "<hr/>";

// E_WARNING，警告级别的错误
settype($var, 'king');

echo "<hr/>";

// 致命错误，使用set_error_handler是无法接管的，页面会显示致命错误
//test();

echo "<hr/>";

echo 'this is a test';

// 手动抛出的致命错误，信息会被显示在浏览器
trigger_error('我是手动抛出的致命错误', E_USER_ERROR);

echo "<hr/>";
echo 'this is a test';
