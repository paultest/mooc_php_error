<?php
/**
 * 下面这个try...catch...是捕获不到除零问题报出的警告错误的
 * User: jiand
 * Date: 2018/11/17
 * Time: 21:19
 */

header('content-type:text/html;charset=utf-8');
error_reporting(-1);

$num = NULL;
try {
    // 除零问题，会报出警告错误，使用异常捕获机制是捕获不到的
    $num = 3 / 0;
    var_dump($num);
} catch (Exception $e) {
    echo $e->getMessage();
    $num = 12;
}

echo '<hr/>';
echo 'continue...';
var_dump($num);