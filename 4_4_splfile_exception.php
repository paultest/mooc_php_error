<?php
/**
 * SplFile的异常捕获
 * User: jiand
 * Date: 2018/11/17
 * Time: 21:19
 */

header('content-type:text/html;charset=utf-8');
error_reporting(-1);

try {
    // 故意写错文件制造异常
    $sqlObj = new SplFileObject('test.txt', 'r');
    echo '<hr/>';
    echo 'continue...';
} catch (Exception $e) {
    echo $e->getMessage();
}

echo '<hr/>';
echo 'continue...';