<?php
/**
 * PDO的异常捕获
 * 注意：使用PDOException或者是直接使用Exception都是可以捕获到的
 * User: jiand
 * Date: 2018/11/17
 * Time: 21:19
 */

header('content-type:text/html;charset=utf-8');
error_reporting(-1);

try {
    // 故意写错用户名制造异常
    $pdo = new PDO('mysql:host=localhost;dbname=mysql', 'r123root', 'root');
    var_dump($pdo);
    echo '<hr/>';
    echo 'continue...';
} catch (PDOException $e) {
    echo $e->getMessage();
}

echo '<hr/>';
echo 'continue...';