<?php
/**
 * error_log函数发送邮件
 * 需要提前设置好邮件
 */

// 设置开启所有的错误
error_reporting(-1);

// 关闭所有的错误显示
ini_set('display_errors', 0);

// 设置开启错误日志记录
ini_set('log_errors', 1);

// 发送邮件
error_log('这个是错误的信息', 1, '844865712@qq.com');