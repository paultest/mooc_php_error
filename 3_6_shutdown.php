<?php
/**
 * register_shutdown_function的例子
 * 如果发生致命错误的话，会提前中止，并且触发register_shutdown_function的回调函数，并且写到日志里面去
 * User: jiand
 * Date: 2018/11/17
 * Time: 20:39
 */

class ShutDown
{
    public function endScript()
    {
        // 如果是错误导致的话，那么打印错误
        if (error_get_last()) {
            echo '<pre>';
            print_r(error_get_last());
            echo '</pre>';
        }

        // 写入错误到日志中去
        // 注意，这里必须要写绝对路径，这是因为register_shutdown_function函数是在内存中调用的，所以相对路径的话找不到的
        file_put_contents('D:\laragon\www\mooc\mooc_php_error\logs\testError.log', 'this is a test');

        // 中止
        die('end script');
    }
}

// 当执行完成或者是中止的时候调用ShutDown类的endScript方法
register_shutdown_function([new ShutDown(), 'endScript']);

// 致命错误
echo md6();

// 这一段是不会打印出来的
echo '<br />';
echo 'have a test';