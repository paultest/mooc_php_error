<?php
/**
 * 触发错误的功能不只限于PHP解释器，还可以通过trigger_error()函数来触发错误
trigger_error用户抛出自定义错误
 */

$num1 = 1;
$num2 = '2a';

// 判断$num1和$num2是否是合法数值
if (!(is_numeric($num1) && is_numeric($num2))) {
	// 抛出通知错误
	trigger_error('num1和num2必须为合法数值', E_USER_NOTICE);

	// 抛出警告错误
	// trigger_error('num1和num2必须为合法数值', E_USER_WARNING);

	// 抛出致命错误
	// trigger_error('num1和num2必须为合法数值', E_USER_ERROR);
} else {
	echo $num1 + $num2;
}

echo PHP_EOL . '程序继续向下执行' . PHP_EOL;
