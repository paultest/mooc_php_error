<?php
/**
 * 将错误日志保存到指定的文件去，关闭页面的错误显示
 * 也可以在php_ini中将error_log配置成指定的文件
 */

// 关闭所有的错误显示
ini_set('display_errors', 'off');

// 设置开启错误日志记录
ini_set('log_errors', 1);

// 错误日志保存到指定文件
ini_set('error_log', './logs/Error.log');

// 设置开启所有的错误
error_reporting(-1);

// Notice 错误
echo $test . PHP_EOL;

// Warning 错误
settype($var, 'king');

echo PHP_EOL;

// 致命错误
test();

echo PHP_EOL . '看看会不会运行' . PHP_EOL;