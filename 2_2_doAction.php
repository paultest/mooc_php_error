<?php
/**
 * 记录登录错误的日志信息
 */

header('content-type:text/html;charset=utf-8');

// 设置区间
ini_set('date.timezone', 'PRC');

// 关闭所有的错误显示
ini_set('display_errors', 0);

// 设置开启所有的错误
error_reporting(-1);

// 设置开启错误日志记录
ini_set('log_errors', 1);

// 错误日志保存到指定文件
ini_set('error_log', './logs/logs.log');

// 忽略重复的错误信息
ini_set('ignore_repeated_errors', 'on');

// 忽略错误消息来源
ini_set('ignore_repeated_source', 'on');

$username = $_POST['name'];
$password = $_POST['password'];

if ($username == 'admin' && $password == 'admin') {
	echo '登录成功';
} else {
	// 记录日志
	$date = date('Y-m-d H:i:s', time());
	$ip = $_SERVER['REMOTE_ADDR'];
	$message = PHP_EOL . "用户 {$username} 在 {$date} 以密码 {$password} 尝试登录系统！IP地址为 {$ip}" . PHP_EOL;
	error_log($message);

	// 跳转到登录页面
	header('location:2_2_form.html');
}