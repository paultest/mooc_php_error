<?php
/**
 * 异常捕获机制是需要主动去抛出异常的
 * User: jiand
 * Date: 2018/11/17
 * Time: 21:19
 */

header('content-type:text/html;charset=utf-8');
error_reporting(-1);

try {
    $num1 = 3;
    $num2 = 0;
    if ($num2 == 0) {
        throw new Exception('0不能当作除数');

        // 这一段是不会显示的，抛出异常后直接跳出去了,不会执行到这里的
        echo 'this is a test';
    } else {
        $res = $num1 / $num2;
    }
} catch (Exception $e) {
    echo $e->getMessage();
}

echo '<hr/>';
echo 'continue...';