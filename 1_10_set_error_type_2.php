<?php
/**
 * 设置错误级别的三种方法：
 *     通过修改PHP配置文件中的error_reporting选项的值
 *     通过error_reporting()函数设置
 *     通过ini_set()函数运行时设置
 */

// 不显示所有的错误（语法错误还是会显示的）
ini_set('error_reporting', 0);

// 显示所有错误(也可以是int_set('error_reportint', 'E_ALL');)
ini_set('error_reporting', -1);

// 不显示所有的错误
ini_set('display_errors', 0);

// Notice错误
echo $test;
echo PHP_EOL;

// 致命错误
echo fun();
echo PHP_EOL;
