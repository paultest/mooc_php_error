<?php
/**
 * error_log可以将日志写入到系统日志中去
 * 在计算机-管理-事件查看器-找到PHP7产生的日志-详细信息可以查看具体的错误
 */

// 设置开启所有的错误
error_reporting(-1);

// 关闭所有的错误显示
ini_set('display_errors', 0);

// 设置开启错误日志记录
ini_set('log_errors', 1);

// 错误日志保存到指定文件
ini_set('error_log', 'syslog');

// 开启记录到系统日志函数
openlog('PHP7.1', LOG_PID, LOG_SYSLOG);

// 记录日志
syslog(LOG_ERR, '这个是测试的错误日志' . date('Y/m/d H:i:s'));

// 关闭系统日志函数
closelog();