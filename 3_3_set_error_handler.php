<?php
/**
 * 自定义错误机制
 * 注意：不能接管致命错误
 */

header('content-type:text/html;charset=utf-8');
error_reporting(-1);

function customError($errno, $errmsg, $file, $line)
{
    if (error_reporting() === 0) {
        return false;
    }

    switch ($errno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    echo "<b>错误类型：</b>{$error}<br/>" . PHP_EOL;
    echo "<b>错误代码：</b>[{$errno}] {$errmsg}<br/>" . PHP_EOL;
    echo "<b>错误代号：</b>{$file}文件中的第{$line}行<br/>" . PHP_EOL;
    echo "<b>PHP版本：</b>" . PHP_VERSION . "(" . PHP_OS . ")<br/>" . PHP_EOL;

    return true;
}

// 接管系统的错误提示机制，第二个参数是错误级别，如果不填的话，默认是所有的错误
set_error_handler('customError');

//过滤notice级别的错误
// set_error_handler("自定义函数名",E_ALL&~E_NOTICE);

// E_NOTICE，通知级别的错误
echo $test;

echo "<hr/>";

// E_WARNING，警告级别的错误
settype($var, 'king');

echo "<hr/>";

// 致命错误，使用set_error_handler是无法接管的
// test();

echo "<hr/>";

// 手动抛出一个错误
trigger_error('this is a test of error', E_USER_ERROR);

echo "<hr/>";

// 使用系统的错误提示机制，取消用户接管的错误提示机制
restore_error_handler();

// E_NOTICE，通知级别的错误
echo $test;

echo "<hr/>";

// 如果需要重新使用自定义错误函数的话，则继续使用set_error_handler("自定义函数名")即可
set_error_handler('customError');

// E_NOTICE，通知级别的错误
echo $test;

echo "<hr/>";


